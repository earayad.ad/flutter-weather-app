import 'package:clima/services/networking.dart';
import 'package:flutter/material.dart';
import 'package:clima/services/location.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'location_screen.dart';
import 'package:clima/services/weather.dart';



class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  double lat;
  double lon;
  bool loading = false;

  void initState() {
    super.initState();
    getlocationData();
  }

  void getlocationData() async {
    try {
      setState(() {
        loading = true;
      });

      WeatherModel weatherModel = WeatherModel();
      var weatherData = await weatherModel.getLocationWeather();

      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return LocationScreen(
          locationWeather: weatherData,
        );
      },),);
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: loading
              ? SpinKitFadingCircle(
                  color: Colors.white,
                  size: 100.0,
                )
              : Text(''),
        ),
      );
  }
}
